#!/bin/sh
if [[ $1 = "c" ]]; then
  bookmark=$(find ~/bookmarks/ -type f | sed -e "s|$HOME/bookmarks/||g" -e 's|.gpg||g' | dmenu)
  cat ~/bookmarks/$bookmark | xclip -i -selection clipboard
elif [[ $1 = "o" ]]; then
  bookmark=$(find ~/bookmarks/ -type f | sed -e "s|$HOME/bookmarks/||g" -e 's|.gpg||g' | dmenu)
  xdg-open $(cat ~/bookmarks/$bookmark)
elif [[ $1 = "s" ]]; then
  url=$(xclip -o)
  file=$(find $HOME/bookmarks/ -type d | sed -e "s|$HOME/bookmarks/||g" -e 's|.gpg||g' | dmenu -p "$url")
  echo $url > $HOME/bookmarks/$file
else
  echo "Invalid argument."
fi
